//alert ('Hello World')

//Mock Database
let posts = [];
//Posts ID
let count = 1

//Add post - eventLister
// This will trigger an event that will add a new post in our database upon clicking the "Create Button"


document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	e.preventDefault();
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector("#txt-body").value
	})
	//count will increment everytime a new post is added
	count ++
	alert("Added Successfuly")
	showPosts()
});


const showPosts = () => {
	//Create a variablie that wil contain all the posts
	let postEntries = "";

	posts.forEach((post) =>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}"> ${post.title}</h3>

				<p id="post-body-${post.id}">${post.body}</p>

				<button onClick ="editPost('${post.id}')">Edit</button>
				<button onClick ="deletePost('${post.id}')">Delete</button>

			</div>
		` 
	});


	console.log(postEntries)

	document.querySelector("#div-post-entries").innerHTML = postEntries

}



//Edit Post Button
// We will create a function that will be called in the onClick() event and will pass the value in the Update Form input box

const editPost = (id) => {
	console.log(id)
	let title = document.querySelector(`#post-title-${id}`).innerHTML;

	let body = document.querySelector(`#post-body-${id}`). innerHTML;


	//Pass the id, title and body from the post to be updated in the edit Post Form
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
};


// Update post - Event Listener
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfuly Updated!');
			break;
		}
	}
});


//ACTIVITY
//Delete post

const deletePost = (id) => {

	for(let i=0; i< posts.length; i++){
		if(posts[i].id.toString() === id){
			posts.splice(i, 1);
		}
	}

	showPosts();

}
